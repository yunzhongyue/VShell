const gulp = require('gulp');
const connect = require('gulp-connect');

gulp.task('serve',(done)=>{
    connect.server({
        root:'.',
        port:1024,
        https:false,
        livereload:true
    });
    done();
})

gulp.task('default',gulp.series('serve'));