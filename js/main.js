//JS加载器
JSLoader={
	loadedJS:[]
};

//判断是否加载
JSLoader.isLoad=function(filePath){
	for(var i=0;i<JSLoader.loadedJS.length;i++){
		if(JSLoader.loadedJS[i]==filePath){
			return true;
		}
	}
	return false;
};

//加载JS
JSLoader.load=function(filePath,callbackFun){
	if(typeof filePath!="undefined"&&""!=filePath){
		if(!JSLoader.isLoad(filePath)){
			$.getScript(filePath,function(){
				JSLoader.loadedJS.push(filePath);
				if(typeof callbackFun=="function"){
					callbackFun();
				}
			});
		}else{
			if(typeof callbackFun=="function"){
				callbackFun();
			}
		}
	}
};

//初始化VShell
$(function(){
	JSLoader.load("./js/src/vShell.js",function(){
		//初始化编辑区域	
		vShell.setEditAndFocus(vShell.promptStr);
		
		//设置按键处理器
		$(".inputArea").keydown(function(event){
			return vShell.keyDownHandle(event);
		});
	});
});