var cmd={
	name:"cmd",
	promptStr:"C:\\Users\\vShell>",
	currDir:["C:","Users","vShell"],
	isDataLoad:false
};

//初始化方法
cmd.initCtx=function(){
	if(!cmd.isDataLoad){
		//加载数据文件
		$.get("./data/windows.data",{},function(data){
			$(".systemData").append(data);
			cmd.isDataLoad=true;
			},"html"); 
	}
};

//获取路径，用数组表示
cmd.getPath=function(filePath){
	if(typeof filePath=="undefined"||filePath=="") return null;
	var filePathArray=filePath.split(/\\/);
	if(filePathArray.length>0){
		var realPath=[];
		var firstFile=filePathArray[0];
		if(/^[A-Z]:$/.test(firstFile)){
			realPath.push(firstFile);
		}else{
			if(/^\.$/.test(firstFile)){
				realPath=realPath.concat(cmd.currDir);
			}else if(/^\.{2}$/.test(firstFile)){
				realPath=realPath.concat(cmd.currDir);
				realPath.pop();
			}else if(/^\w+$/.test(firstFile)){
				realPath=realPath.concat(cmd.currDir);
				realPath.push(firstFile);
			}else{
				realPath.push(firstFile);
			}
		}
		for(var i=1;i<filePathArray.length;i++){
			if(/^\.$/.test(filePathArray[i])){
				continue;
			}else if(/^\.{2}$/.test(filePathArray[i])){
				realPath.pop();
			}else{
				realPath.push(filePathArray[i]);
			}
		}
		return realPath;
	}
};

//获取文件对象
cmd.getFile=function(realPath){
	if(typeof realPath!="undefined"){
		var xPath=".systemData>windows>disks>disk[letter='"+realPath[0]+"']>";
		for(var i=1;i<realPath.length;i++){
			xPath+="file[type='dir'][name='"+realPath[i]+"']>";
		}
		return $(xPath.substr(0,xPath.length-1));
	}
};

//判断文件是否存在
cmd.isFileExist=function(realPath){
	if(typeof realPath!="undefined"){
		var xFile=cmd.getFile(realPath);
		if(xFile.length>0){
			return true;
		}
	}
	return false;
};

//命令处理器
cmd.commandHandle={
	md:function(params){
		if(typeof params=='undefined'||params.length==0){
			vShell.showInfo("命令语法不正确。");
			vShell.setEditAndFocus(cmd.promptStr);
		}else{
			for(var i=0;i<params.length;i++){
				var realPath=cmd.getPath(params[i]);
				if(cmd.isFileExist(realPath)){
					vShell.showInfo("子目录或文件 "+params[i]+" 已经存在。");
					vShell.showInfo("处理： "+params[i]+" 时出错。");
				}else{
					var filePath=[];
					for(var n=0;n<realPath.length;n++){
						filePath.push(realPath[n]);
						if(cmd.isFileExist(filePath)){
							continue;
						}else{
							var fileName=filePath.pop();
							var parentDir=cmd.getFile(filePath);
							parentDir.append("<file name='"+fileName+"' type='dir' createTime='"+util.formatDate(new Date(),"yyyy-MM-dd hh:mm:ss")+"'></file>");
						}
					}
				}
			}
			vShell.setEditAndFocus(cmd.promptStr);
		}
		return false;
	},
	cd:function(param){
		if(param.length!=1){
			vShell.showInfo("系统找不到指定的路径。");
		}else{
			var path=param[0];
			var realPath=cmd.getPath(path);
			if(realPath.length>0){
				if(cmd.isFileExist(realPath)){
					cmd.currDir=realPath;
					cmd.promptStr=realPath.join("\\")+">";
				}else{
					vShell.showInfo("系统找不到指定的路径。");
				}
			}
		}
		vShell.setEditAndFocus(cmd.promptStr);
		return false;
	},
	dir:function(params){
		var showFileList=function(dirPath){
			var selectDisk=$(".systemData>windows>disks>disk[letter='"+dirPath[0]+"']");
			vShell.showInfo("驱动器 "+selectDisk.attr("letter")+" 中的卷是 "+selectDisk.attr("name")+"<br/>卷的序列号是 "+selectDisk.attr("seqNum"));
			
			var listInfo="<table>";
			var selectDir=cmd.getFile(dirPath);
			var parentDir=selectDir.parent();
			if(selectDir.is("file")){
				listInfo+="<tr><td>"+$(selectDir).attr("createTime")+"</td><td>"+$(selectDir).attr("type")+"</td><td>.</td></tr>";
			}
			if(parentDir.is("file")){
				listInfo+="<tr><td>"+$(parentDir).attr("createTime")+"</td><td>"+$(parentDir).attr("type")+"</td><td>..</td></tr>";
			}
			selectDir.children("file").each(function(i){
				listInfo+="<tr><td>"+$(this).attr("createTime")+"</td><td>"+$(this).attr("type")+"</td><td>"+$(this).attr("name")+"</td></tr>";
			});
			var fileCount=selectDir.children("file[type='file']").length;
			var dirCount=selectDir.children("file[type='dir']").length;
			listInfo+="<tr><td></td><td> "+fileCount+" 个文件</td><td>"+fileCount*Math.floor(Math.random()*(1000+1))+" 字节</td></tr>";
			listInfo+="<tr><td></td><td>"+dirCount+" 个目录</td><td>"+dirCount*Math.floor(Math.random()*(1000+1))+" 可用字节</td></tr>";
			listInfo+="</table>";
			vShell.showInfo(listInfo);
		};
		var realPath=[];
		if(typeof params=="undefined"||params.length==0){
			realPath=cmd.currDir;
			showFileList(realPath);
		}else{
			for(var i=0;i<params.length;i++){
				realPath=cmd.getPath();
				if(cmd.isFileExist(realPath)){
					showFileList(realPath);
				}else{
					vShell.showInfo("找不到文件");
				}
			}
		}
		vShell.setEditAndFocus(cmd.promptStr);
		return false;
	}
};

//按键事件监听
cmd.keyDownHandle=function(event){
		if(event.keyCode==13){
		/*
		 * 回车键
		 */
		var command=vShell.getInput();
		vShell.showInfo(command);
		command=command.replace(cmd.promptStr,"");
		if("exit"!=command){
			if(/^[A-Z]:$/.test(command)){//处理磁盘切换，例如：输入D:时切换到D盘
				if(cmd.isFileExist([command])){
					cmd.currDir=[command];
					cmd.promptStr=command+">";
				}else{
					vShell.showInfo("系统找不到指定的驱动器。");
				}
				vShell.setEditAndFocus(cmd.promptStr);
			}else{
				var cmdArray=command.split(/\s+/);
				var cmdName=cmdArray.shift();
				var cmdParams=cmdArray;
				if(typeof cmd.commandHandle[cmdName]=='function'){
					return cmd.commandHandle[cmdName](cmdParams);
				}else{
					vShell.showInfo("'"+cmdName+"' 不是内部或外部命令，也不是可运行的程序或批处理文件。");
					vShell.setEditAndFocus(cmd.promptStr);
					return false;
				}
			}
		}else{
			vShell.currCtx=null;
			vShell.setEditAndFocus(vShell.promptStr);
		}
		
		return false;
	}else if(event.keyCode==8){
		/*
		 * 删除键
		 */
		if(vShell.getInput()==cmd.promptStr){
			return false;
		}
	}
};
