var calc={
	name:"calc",
	promptStr:"Calc>"
};

//初始化方法
calc.initCtx=function(){
	
};

//按键事件监听
calc.keyDownHandle=function(event){
	if(event.keyCode==13){
		/*
		 * 回车键
		 */
		var command=vShell.getInput();
		vShell.showInfo(command);
		command=command.replace(calc.promptStr,"");
		if("exit"!=command){
			if(command.match(/\d+([+|-|*|\/]\d+)*/)){
				try{
					eval("var _calcResult="+command);
					vShell.showInfo(command+" = "+_calcResult);
				}catch(e){
					vShell.showInfo("无效的表达式！");
				}
			}else{
				vShell.showInfo("暂不支持计算此类的表达式！");
			}
			vShell.setEditAndFocus(calc.promptStr);
		}else{
			vShell.currCtx=null;
			vShell.setEditAndFocus(vShell.promptStr);
		}
		
		return false;
	}else if(event.keyCode==8){
		/*
		 * 删除键
		 */
		if(vShell.getInput()==calc.promptStr){
			return false;
		}
	}
};
